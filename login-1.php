<?php include 'header.php' ?>


<div class="page">
  <div class="forms">
    <div class="logo">
      <a href="#"><img src="images/logo_1.png" alt=""></a>
    </div>

    <div id="Login">
      <form action="">
        <h1 class="title">Login</h1>
        <div class="col-md-12">
          <div class="form-group">
            <div class="input-group">
              <input type="text" class="form-control" name="" placeholder="User">
              <span class="input-group-addon"><i class="fa fa-user"></i></span>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
              <input type="password" class="form-control" name="">
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            </div>
          </div>

          <div class="form-group">
            <div class="form-check">
              <label>
                <input type="checkbox" name="" value="1"> <span class="label-text">Keep me signed in</span>
              </label>
            </div>
            <div class="form-check pull-right">
              <button id="login_lost_btn" type="button" class="btn-link">forgot pass?</button>
            </div>
          </div>
          <div class="form-group text-center">
            <button type="submit" class="btn btn-primary">Login</button>
          </div>
          <div class="form-group text-center">
            <p>Don't have an account? <button id="register_btn" type="button" class="btn btn-link">Register Now</button>
            </p>
          </div>
        </div>
      </form>
    </div>

    <div id="vendor-re">
      <h1 class="title">Registration</h1>
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#User">User</a></li>
        <li><a data-toggle="tab" href="#Vendor">Vendor</a></li>
      </ul>
      <div class="tab-content">
        <div id="User" class="tab-pane fade in active">
          <form action="">
            <div class="col-md-12">
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="text" class="form-control" name="" placeholder="First Name">
                      <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="text" class="form-control" name="" placeholder="Last Name">
                      <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                  </div>
                </div>
              </div><!-- ./form-group -->

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="text" class="form-control" name="" placeholder="Phone">
                      <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="email" class="form-control" name="" placeholder="Email">
                      <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    </div>
                  </div>
                </div>
              </div><!-- ./form-group -->

              <div class="form-group">
                <div class="row">
                  <div class="col-md-12">
                    <div class="input-group">
                      <textarea class="form-control" name="" placeholder="Address"></textarea>
                      <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                    </div>
                  </div>
                </div>
              </div><!-- ./form-group -->

              <div class="form-group">
                <div class="row">
                  <div class="col-md-12">
                    <div class="input-group">
                      <input type="password" class="form-control" name="" placeholder="Password">
                      <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    </div>
                  </div>
                </div>
              </div><!-- ./form-group -->

              <div class="form-group">
                <div class="row">
                  <div class="col-md-12">
                    <div class="input-group">
                      <input type="password" class="form-control" name="" placeholder="Confirm Password">
                      <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    </div>
                  </div>
                </div>
              </div><!-- ./form-group -->
              <div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
        <!-- ./User -->
        <div id="Vendor" class="tab-pane fade">
          <form action="">
            <div class="col-md-12">
              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="text" class="form-control" name="" placeholder="First Name">
                      <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="text" class="form-control" name="" placeholder="Last Name">
                      <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                  </div>
                </div>
              </div><!-- ./form-group -->

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6 add-phoneNo">
                    <div class="input-group phone-a">
                      <input type="text" class="form-control" name="" placeholder="Phone">
                      <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                      <span class="input-group-addon" id="add"><i class="fa fa-plus"></i></span>
                    </div>
                    <!-- <div class="add-phone"></div> -->

                  </div>

                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="email" class="form-control" name="" placeholder="Email">
                      <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    </div>
                  </div>
                </div>
              </div><!-- ./form-group -->

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="text" class="form-control" name="" placeholder="Company Name">
                      <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="email" class="form-control" name="" placeholder="Company Email">
                      <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    </div>
                  </div>
                </div>
              </div><!-- ./form-group -->

              <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="text" class="form-control" name="" placeholder="Company Phone">
                      <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="input-group">
                      <input type="text" class="form-control" name="" placeholder="Map">
                      <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                    </div>
                  </div>
                </div>
              </div><!-- ./form-group -->

              <div class="form-group">
                <div class="row">
                  <div class="col-md-12">
                    <div class="input-group">
                      <textarea class="form-control" name="" placeholder="Address"></textarea>
                      <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                    </div>
                  </div>
                </div>
              </div><!-- ./form-group -->
              <h4>Add vehicle <span id="AddMore">Add More</span></h4>
              <div class="addVehicle">

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <select class="form-control">
                        <option>Select Type</option>
                        <option>LOGISTICS</option>
                        <option>BUS</option>
                        <option>CAR/TAXI</option>
                        <option>AMBULANCE</option>
                      </select>
                    </div>

                    <div class="col-md-6">
                      <input type="text" class="form-control" name="" placeholder="Truck No">
                    </div>
                  </div>
                </div><!-- ./form-group -->

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <input type="text" class="form-control" name="" placeholder="RC">
                    </div>

                    <div class="col-md-6">
                      <input type="text" class="form-control" name="" placeholder="">
                    </div>
                  </div>
                </div><!-- ./form-group -->


              </div>
              <div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <!-- ./tab-content -->

    </div>
    <!-- /.vendor -->

    <div id="ForgotPassword">
      <form action="">
        <h1 class="title">Forgot password</h1>
        <div class="col-md-12">
          <div class="form-group">
            <div class="input-group">
              <input type="email" class="form-control" name="" placeholder="Enter Email">
              <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            </div>
          </div>
          <div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
                <p>Have an account? <button id="login_btn" type="button" class="btn btn-link">Login Now</button>
            </p>
              </div>
        </div>
      </form>
    </div>
    <!-- ./ForgotPassword -->
  </div>

</div>

<script>
  $(document).ready(function () {
    $("#vendor-re").hide(1000);
    $("#ForgotPassword").hide(1000);
    
    $("#register_btn").click(function () {
      $("#vendor-re").show(1000);
      $("#Login").hide(1000);
    });
    $("#login_lost_btn").click(function () {
      $("#ForgotPassword").show(1000);
      $("#Login").hide(1000);
      $("#vendor-re").hide(1000);
    });

    $("#login_btn").click(function () {
      $("#ForgotPassword").hide(1000);
      $("#Login").show(1000);
      $("#vendor-re").hide(1000);
    });
    
  });

  // for append and delete elements
  $(document).ready(function () {
    var i = 1;
    var j= 1;
    $("#add").click(function () {
      
      $(".add-phoneNo").append('<div id="add-phone' + i +
        '" class="phone-space" ><div class="input-group"><input type="text" class="form-control" name="" placeholder="Phone"><span class="input-group-addon"><i class="fa fa-phone"></i></span><span class="input-group-addon delete" id="' +
        i + '"><i class="fa fa-minus"></i></span></div></div>');
    });
    
    i++;

    $("#AddMore").click(function () {
      j++;
      $(".addVehicle").append('<div id="Vehicle' + j + '"><i class="fa fa-times V-delete" id="' + j +
        '"></i><div class="form-group"><div class="row"><div class="col-md-6"><select class="form-control"><option>Select Type</option><option>4 Wheeler</option><option>10 Wheeler</option></select></div><div class="col-md-6"><input type="text" class="form-control" name="" placeholder="Truck No"></div></div></div><div class="form-group"><div class="row"><div class="col-md-6"><input type="text" class="form-control" name="" placeholder="RC"></div><div class="col-md-6"><input type="text" class="form-control" name="" placeholder=""></div></div></div></div>'
      )

    });

  });


  $(document).on('click', '.delete', function () {
    var button_id = $(this).attr("id");
    $('#add-phone' + button_id + '').remove();
  });

  $(document).on('click', '.V-delete', function () {
    var button_id = $(this).attr("id");
    $('#Vehicle' + button_id + '').remove();
  });
  // $(document).ready(function(){
  //     $("#delete").click(function(){
  //         $(".input-group").remove();
  //     });
  // });
</script>


<?php include 'footer.php' ?>