<?php     
include_once 'config.php' ;
include_once "header1.php";
if(!isset($_SESSION['email'])){
    header('Location:login.php');
}
    $sql = "SELECT * FROM vehicles where is_active = 1";    
    $result =   mysqli_query($conn, $sql);    
    $vehicles = array();

    while($data = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
    //will output all data on each loop.
    $vehicles[] = array('id'=>$data['id'],'name'=>$data['name']); 
   }

   //check if form1 submitted

    if(isset($_POST['submit_step1']))
    {   
        $_SESSION['user_id']            = $_POST['user_id'];
        $_SESSION['pickuplocation']     = $_POST['pickuplocation'];
        $_SESSION['pickuplocation_Lat'] = $_POST['pickuplocation_Lat'];
        $_SESSION['pickuplocation_Lng'] = $_POST['pickuplocation_Lng'];
        $_SESSION['droplocation']       = $_POST['droplocation'];
        $_SESSION['droplocation_Lat']   = $_POST['droplocation_Lat'];
        $_SESSION['droplocation_Lng']   = $_POST['droplocation_Lng'];
        $_SESSION['vehicle_type']       = $_POST['vehicle_type'];
        $_SESSION['phone']              = $_POST['phone'];
        $_SESSION['num_of_truck']       = $_POST['num_of_truck'];
        $_SESSION['totalAmount']        = $_POST['totalAmount'];

        header("location:confirm_details.php");
    }
    
 if($_SESSION['email']!= ''){ 
                $vendor_id=$_SESSION['vendor_id'];
                $email=$_SESSION['email'];
               $sql_car="SELECT *,f.id AS enqId, f.status AS enq_status FROM `front_queries` f INNER JOIN transaction t ON f.id = t.order_id WHERE  t.order_status = 'success' AND f.type = 'car' ORDER BY f.id desc";
                $result_car=mysqli_query($conn,$sql_car);

                $sql_bus="SELECT *,f.id AS enqId, f.status AS enq_status FROM `front_queries` f INNER JOIN transaction t ON f.id = t.order_id WHERE  t.order_status = 'success' AND f.type = 'bus' ORDER BY f.id desc";
                $result_bus=mysqli_query($conn,$sql_bus);

                $sql_ambulance="SELECT *,f.id AS enqId, f.status AS enq_status FROM `front_queries` f INNER JOIN transaction t ON f.id = t.order_id WHERE  t.order_status = 'success' AND f.type = 'ambulance' ORDER BY f.id desc";
                $result_ambulance=mysqli_query($conn,$sql_ambulance);

                $sql_logistics="SELECT *,f.id AS enqId, f.status AS enq_status FROM `front_queries` f INNER JOIN transaction t ON f.id = t.order_id WHERE  t.order_status = 'success' AND f.type = 'truck' ORDER BY f.id desc";
                $result_logistics=mysqli_query($conn,$sql_logistics);

                 // $resultset=array();
    }
                                    
   if(isset($_POST['bid_submit'])){
       $sql2="INSERT INTO bid (enq_id, vendor_id, bid_amount,comment) VALUES ('".$_POST["enquiry_id"]."', '".$_SESSION['vendor_id']."', '".$_POST["bid_amount"]."','".$_POST["bid_comment"]."')";
       $result=mysqli_query($conn, $sql2);
        // header("location:vendor_dashboard.php");
   }

    ?>
<style>
  .vendor {    
    background: #fff;
    margin: 10% 0 8% 0;
    padding: 5px;
}
.banner {

   padding-bottom: 0px;

}
    </style>

<!--Banner-->
<section class="banner">
  <!--Background-->
  <div class="bg-parallax bg-1"></div>
  <!--End Background-->
  <section>
    <div class="container">
      <div class="row">

        <div class="user-d">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#car">Car</a></li>
            <li><a data-toggle="tab" href="#bus">Bus</a></li>
            <li><a data-toggle="tab" href="#ambulance">Ambulance</a></li>
            <li><a data-toggle="tab" href="#logistics">Logistics</a></li>
          </ul>

          <div class="tab-content">
            <div id="car" class="tab-pane fade in active">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Sr.No</th>
                      <th>Enquiry_Id</th>
                      <th>Source Point</th>
                      <th>Destination</th>
                      <th>Date</th>
                      <th>Vehicle Type</th>
                      <th>Bid</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i=1; while($row_car=mysqli_fetch_array($result_car))
                    { if($row_car['enq_status']=='0') {?>
                    <tr>
                      <td>
                        <?php  echo $i;?>
                      </td>
                      <td>
                        <?php echo $row_car['enqId']; ?>
                      </td>
                      <td>
                        <?php echo $row_car['prepickuplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_car['predroplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_car['predateofpickup']; ?>
                      </td>
                      <td>
                        <?php echo $row_car['prevehicletype']; ?>
                      </td>
                      <td><button type="button" class="btn btn-primary bid" enquiry_id="<?php echo $row_car['enqId']; ?>"
                          data-toggle="modal" data-target="#myModal" data-placement="left" title="View Bid"><i class="fa fa-eye"></i></button></td>
                    </tr>
                    <?php $i++; } else {
            "Select * from bid where vendor_id=$vendor_id and enq_id=$row_car[enqId] and status='1'";
            $checkBid = mysqli_query($conn,"Select * from bid where vendor_id=$vendor_id and enq_id=$row_car[enqId] and status='1'");
            if(mysqli_num_rows($checkBid)>0){?>
                    <tr>
                      <td>
                        <?php  echo $i;?>
                      </td>
                      <td>
                        <?php echo $row_car['enqId']; ?>
                      </td>
                      <td>
                        <?php echo $row_car['prepickuplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_car['predroplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_car['predateofpickup']; ?>
                      </td>
                      <td>
                        <?php echo $row_car['prevehicletype']; ?>
                      </td>
                      <td><button type="button" class="btn btn-success customer_detail" enquiry_id="<?php echo $row_car['enqId']; ?>"
                          data-toggle="modal" data-target="#customer_detail" data-placement="left" title="View Customer Details"><i
                            class="fa fa-eye"></i></button></td>
                    </tr>
                    <?php  }
        } $i++;}?>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- ./car -->
            <div id="bus" class="tab-pane fade">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Sr.No</th>
                      <th>Enquiry_Id</th>
                      <th>Source Point</th>
                      <th>Destination</th>
                      <th>Date</th>
                      <th>Vehicle Type</th>
                      <th>Bid</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i=1; while($row_bus=mysqli_fetch_array($result_bus))
                    { if($row_bus['enq_status']=='0') {?>
                    <tr>
                      <td>
                        <?php  echo $i;?>
                      </td>
                      <td>
                        <?php echo $row_bus['enqId']; ?>
                      </td>
                      <td>
                        <?php echo $row_bus['prepickuplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_bus['predroplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_bus['predateofpickup']; ?>
                      </td>
                      <td>
                        <?php echo $row_bus['prevehicletype']; ?>
                      </td>
                      <td><button type="button" class="btn btn-primary bid" enquiry_id="<?php echo $row_bus['enqId']; ?>"
                          data-toggle="modal" data-target="#myModal" data-placement="left" title="View Bid"><i class="fa fa-eye"></i></button></td>
                    </tr>
                    <?php $i++; } else {
            "Select * from bid where vendor_id=$vendor_id and enq_id=$row_bus[enqId] and status='1'";
            $checkBid = mysqli_query($conn,"Select * from bid where vendor_id=$vendor_id and enq_id=$row_bus[enqId] and status='1'");
            if(mysqli_num_rows($checkBid)>0){?>
                    <tr>
                      <td>
                        <?php  echo $i;?>
                      </td>
                      <td>
                        <?php echo $row_bus['enqId']; ?>
                      </td>
                      <td>
                        <?php echo $row_bus['prepickuplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_bus['predroplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_bus['predateofpickup']; ?>
                      </td>
                      <td>
                        <?php echo $row_bus['prevehicletype']; ?>
                      </td>
                      <td><button type="button" class="btn btn-success customer_detail" enquiry_id="<?php echo $row_bus['enqId']; ?>"
                          data-toggle="modal" data-target="#customer_detail" data-placement="left" title="View Customer Details"><i
                            class="fa fa-eye"></i></button></td>
                    </tr>
                    <?php  }
        } $i++;}?>
                  </tbody>
                </table>
              </div>

            </div>
            <!-- ./bus -->
            <div id="ambulance" class="tab-pane fade">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Sr.No</th>
                      <th>Enquiry_Id</th>
                      <th>Source Point</th>
                      <th>Destination</th>
                      <th>Date</th>
                      <th>Vehicle Type</th>
                      <th>Bid</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i=1; while($row_ambulance=mysqli_fetch_array($result_ambulance))
                    { if($row_ambulance['enq_status']=='0') {?>
                    <tr>
                      <td>
                        <?php  echo $i;?>
                      </td>
                      <td>
                        <?php echo $row_ambulance['enqId']; ?>
                      </td>
                      <td>
                        <?php echo $row_ambulance['prepickuplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_ambulance['predroplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_ambulance['predateofpickup']; ?>
                      </td>
                      <td>
                        <?php echo $row_ambulance['prevehicletype']; ?>
                      </td>
                      <td><button type="button" class="btn btn-primary bid" enquiry_id="<?php echo $row_ambulance['enqId']; ?>"
                          data-toggle="modal" data-target="#myModal" data-placement="left" title="View Bid"><i class="fa fa-eye"></i></button></td>
                    </tr>
                    <?php $i++; } else {
            "Select * from bid where vendor_id=$vendor_id and enq_id=$row_ambulance[enqId] and status='1'";
            $checkBid = mysqli_query($conn,"Select * from bid where vendor_id=$vendor_id and enq_id=$row_ambulance[enqId] and status='1'");
            if(mysqli_num_rows($checkBid)>0){?>
                    <tr>
                      <td>
                        <?php  echo $i;?>
                      </td>
                      <td>
                        <?php echo $row_ambulance['enqId']; ?>
                      </td>
                      <td>
                        <?php echo $row_ambulance['prepickuplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_ambulance['predroplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_ambulance['predateofpickup']; ?>
                      </td>
                      <td>
                        <?php echo $row_ambulance['prevehicletype']; ?>
                      </td>
                      <td><button type="button" class="btn btn-success customer_detail" enquiry_id="<?php echo $row_ambulance['enqId']; ?>"
                          data-toggle="modal" data-target="#customer_detail" data-placement="left" title="View Customer Details"><i
                            class="fa fa-eye"></i></button></td>
                    </tr>
                    <?php  }
        } $i++;}?>
                  </tbody>
                </table>
              </div>

            </div>
            <!-- ./ambulance -->
            <div id="logistics" class="tab-pane fade">
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Sr.No</th>
                      <th>Enquiry_Id</th>
                      <th>Source Point</th>
                      <th>Destination</th>
                      <th>Date</th>
                      <th>Vehicle Type</th>
                      <th>Bid</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i=1; while($row_logistics=mysqli_fetch_array($result_logistics))
                    { if($row_logistics['enq_status']=='0') {?>
                    <tr>
                      <td>
                        <?php  echo $i;?>
                      </td>
                      <td>
                        <?php echo $row_logistics['enqId']; ?>
                      </td>
                      <td>
                        <?php echo $row_logistics['prepickuplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_logistics['predroplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_logistics['predateofpickup']; ?>
                      </td>
                      <td>
                        <?php echo $row_logistics['prevehicletype']; ?>
                      </td>
                      <td><button type="button" class="btn btn-primary bid" enquiry_id="<?php echo $row_logistics['enqId']; ?>"
                          data-toggle="modal" data-target="#myModal" data-placement="left" title="View Bid"><i class="fa fa-eye"></i></button></td>
                    </tr>
                    <?php $i++; } else {
            "Select * from bid where vendor_id=$vendor_id and enq_id=$row_logistics[enqId] and status='1'";
            $checkBid = mysqli_query($conn,"Select * from bid where vendor_id=$vendor_id and enq_id=$row_logistics[enqId] and status='1'");
            if(mysqli_num_rows($checkBid)>0){?>
                    <tr>
                      <td>
                        <?php  echo $i;?>
                      </td>
                      <td>
                        <?php echo $row_logistics['enqId']; ?>
                      </td>
                      <td>
                        <?php echo $row_logistics['prepickuplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_logistics['predroplocation']; ?>
                      </td>
                      <td>
                        <?php echo $row_logistics['predateofpickup']; ?>
                      </td>
                      <td>
                        <?php echo $row_logistics['prevehicletype']; ?>
                      </td>
                      <td><button type="button" class="btn btn-success customer_detail" enquiry_id="<?php echo $row_logistics['enqId']; ?>"
                          data-toggle="modal" data-target="#customer_detail" data-placement="left" title="View Customer Details"><i
                            class="fa fa-eye"></i></button></td>
                    </tr>
                    <?php  }
        } $i++;}?>
                  </tbody>
                </table>
              </div>

            </div>
            <!-- ./logistics -->
          </div>
          <!-- ./tab-content -->
        </div>
        <!-- ./user-d -->

      </div>
    </div>
  </section>
  <?php include 'footer.php' ?>
  <!-- Modal Logistics Start -->
  <div id="myModalLogistics" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Vehicle Type</h4>
        </div>
        <div class="modal-bodyscrol">
          <div class="modal-body">

            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-1.jpg" alt="logi-1">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      Champion
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-2.jpg" alt="logi-2">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      ACE
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-3.jpg" alt="logi-3">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      Chota Hathi
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-4.jpg" alt="logi-4">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      Turbo
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-5.jpg" alt="logi-5">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      LTP 1613
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-6.jpg" alt="logi-6">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      LGC 1518
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-7.jpg" alt="logi-7">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      Eicher 14 feet
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-8.jpg" alt="logi-8">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      Eicher 19 feet
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-9.jpg" alt="logi-9">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      Eicher 17 feet
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-10.jpg" alt="logi-10">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      20 feet closed
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-11.jpg" alt="logi-11">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      9 ton / 6 wheel
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-12.jpg" alt="logi-12">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      16 ton / 10 wheel
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-13.jpg" alt="logi-13">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      21 ton / 12 wheel
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-14.jpg" alt="logi-14">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      25 ton / 14 wheel
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="col-xs-6 col-md-4">
              <div class="sales-item modl-item">
                <figure class="home-sales-img">

                  <img src="assets/images/logi-6.jpg" alt="logi-6">

                  <figcaption>

                  </figcaption>
                </figure>
                <div class="home-sales-text">
                  <div class="home-sales-name-places">
                    <div class="home-sales-name">
                      LGC 1518
                    </div>
                  </div>

                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal Logistics end -->
  <!-- Modal taxi Start -->
  <div id="myModaltaxi" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Taxi</h4>
        </div>
        <div class="modal-body">

          <div class="col-xs-6 col-md-4">
            <div class="sales-item modl-item">
              <figure class="home-sales-img">

                <img src="assets/images/ambulance.jpg" alt="ambulance">

                <figcaption>

                </figcaption>
              </figure>
              <div class="home-sales-text">
                <div class="home-sales-name-places">
                  <div class="home-sales-name">
                    Ambulance
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div class="col-xs-6 col-md-4">
            <div class="sales-item modl-item">
              <figure class="home-sales-img">

                <img src="assets/images/obituary.jpg" alt="obituary">

                <figcaption>

                </figcaption>
              </figure>
              <div class="home-sales-text">
                <div class="home-sales-name-places">
                  <div class="home-sales-name">
                    Obituary
                  </div>
                </div>

              </div>
            </div>
          </div>
          <!-- <div class="col-xs-6 col-md-4">
                            <div class="sales-item modl-item">
                                <figure class="home-sales-img">
                                    
                                    <img src="assets/images/taxi-1.jpg" alt="">
                                    
                                    <figcaption>
                                    
                                    </figcaption>
                                </figure>
                                <div class="home-sales-text">
                                    <div class="home-sales-name-places">
                                        <div class="home-sales-name">
                                            Maruti Suzuki Ciaz
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                            <div class="sales-item modl-item">
                                <figure class="home-sales-img">
                                    
                                    <img src="assets/images/taxi-2.jpg" alt="">
                                    
                                    <figcaption>
                                    
                                    </figcaption>
                                </figure>
                                <div class="home-sales-text">
                                    <div class="home-sales-name-places">
                                        <div class="home-sales-name">
                                            Honda
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div> -->
          <div class="col-xs-6 col-md-4">
            <div class="sales-item modl-item">
              <figure class="home-sales-img">

                <img src="assets/images/taxi-3.jpg" alt="taxi-3">

                <figcaption>

                </figcaption>
              </figure>
              <div class="home-sales-text">
                <div class="home-sales-name-places">
                  <div class="home-sales-name">
                    Audi Q4
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div class="col-xs-6 col-md-4">
            <div class="sales-item modl-item">
              <figure class="home-sales-img">

                <img src="assets/images/taxi-4.jpg" alt="taxi-4">

                <figcaption>

                </figcaption>
              </figure>
              <div class="home-sales-text">
                <div class="home-sales-name-places">
                  <div class="home-sales-name">
                    Toyota Yaris iA
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div class="col-xs-6 col-md-4">
            <div class="sales-item modl-item">
              <figure class="home-sales-img">

                <img src="assets/images/taxi-5.jpg" alt="taxi-5">

                <figcaption>

                </figcaption>
              </figure>
              <div class="home-sales-text">
                <div class="home-sales-name-places">
                  <div class="home-sales-name">
                    Toyota EZ
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div class="col-xs-6 col-md-4">
            <div class="sales-item modl-item">
              <figure class="home-sales-img">

                <img src="assets/images/taxi-6.jpg" alt="taxi-6">

                <figcaption>

                </figcaption>
              </figure>
              <div class="home-sales-text">
                <div class="home-sales-name-places">
                  <div class="home-sales-name">
                    BMW 5 Series
                  </div>
                </div>

              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal taxi end -->


  <script type="text/javascript">
    // Wait for the DOM to be ready
    $(function () {
      // Initialize form validation on the registration form.
      // It has the name attribute "registration"

      $(document).on('click', '.bid', function () {
        var enquiry_id = $(this).attr('enquiry_id');

        $('#enquiry_id').val(enquiry_id);
        $('#bidenqid').text('( Enquiry Id - ' + enquiry_id + ' )');
      });

      $(document).on('click', '.customer_detail', function () {
        var enquiry_id = $(this).attr('enquiry_id');
        $.ajax({
          data: {
            "enquiry_id": enquiry_id
          },
          url: "customer_detail.php",
          type: 'POST',
          success: function (data) {
            $("#bid_accept").html(data);
          }
        });
      });
      $("form[name='bookLogistickForm']").validate({
        // Specify validation rules
        rules: {
          pickuplocation: "required",
          droplocation: "required",
          phone: {
            required: true,
            number: true,
            minlength: 10
          },
          vehicle_type: "required",
          num_of_truck: {
            required: true,
            number: true
          },

        },
        // Specify validation error messages
        messages: {
          pickuplocation: "Enter your pickuplocation",
          droplocation: "Enter your droplocation",
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function (form) {
          form.submit();
        }
      });

      $('#num_of_truck').keyup(function (e) {

        var totalAmount = 0;
        var num_of_truck = $('#num_of_truck').val();
        var charges_per_truck = 200;
        totalAmount = num_of_truck * charges_per_truck;

        $('#totalAmount').val(totalAmount);

      });

    });
  </script>


  <script type="text/javascript">
    function initialize_auto() {
      var options = {
        types: ['(cities)'],
        componentRestrictions: {
          country: "IN"
        }
      };

      var pickuplocation = document.getElementById('pickuplocation');
      var autocomplete = new google.maps.places.Autocomplete(pickuplocation, options);
      google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        document.getElementById('pickuplocation_Lat').value = place.geometry.location.lat();
        document.getElementById('pickuplocation_Lng').value = place.geometry.location.lng();

      });

      var droplocation = document.getElementById('droplocation');
      var autocomplete2 = new google.maps.places.Autocomplete(droplocation, options);
      google.maps.event.addListener(autocomplete2, 'place_changed', function () {
        var place2 = autocomplete2.getPlace();
        document.getElementById('droplocation_Lat').value = place2.geometry.location.lat();
        document.getElementById('droplocation_Lng').value = place2.geometry.location.lng();

      });
    }

    google.maps.event.addDomListener(window, 'load', initialize_auto);

    function validateNumber(event) {
      var key = window.event ? event.keyCode : event.which;
      if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
      } else if (key < 48 || key > 57) {
        return false;
      } else {
        return true;
      }
    };
  </script>

  <script>
    $(document).ready(function(){
    $('[data-toggle="modal"]').tooltip();
});
</script>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Bid <span id="bidenqid"></span></h4>
        </div>
        <form action="" method="post">
          <div class="modal-body">

            <div class="form-group">
              <label>Bid Amount</label>
              <input type="hidden" name="enquiry_id" id="enquiry_id">
              <input type="text" class="form-control" name="bid_amount" placeholder="Bid Amount">
            </div>
            <div class="form-group">
              <label>Comments</label>
              <textarea class="form-control" placeholder="Enter Your Message.." name="bid_comment"></textarea>
            </div>
            <!--  <div class="form-group text-right">
                 <button type="submit" class="btn btn-primary"></button>
             </div> -->
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary" name="bid_submit">Submit</button>

          </div>
        </form>
      </div>

    </div>
  </div>
  <!-- bid_accept_Modal -->
  <div class="modal fade" id="customer_detail" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Enquiry</h4>
        </div>
        <div class="modal-body">
          <div class="table-responsive">
            <table class="table table-hover table-border">
              <thead>
                <tr>
                  <th>Sr.no</th>
                  <th>Enquiry Id</th>
                  <!-- <th>Transporter Id</th> -->
                  <th>Customer Name</th>
                  <th>Contact No</th>
                </tr>
              </thead>
              <tbody id="bid_accept">

              </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  </div>